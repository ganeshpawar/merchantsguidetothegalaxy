using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.App.Tests.FileWrapper
{
    [TestFixture()]
    public class GivenAValidPathAndFileDoesNotExists
    {
        private MerchantGuideToTheGalaxy.App.FileWrapper _fileWrapper;
        private string _path;

        [SetUp]
        public void SetUp()
        {
            _fileWrapper = new MerchantGuideToTheGalaxy.App.FileWrapper();
            _path = TestContext.CurrentContext.TestDirectory + @"\FileWrapper\Test2.txt";
        }

        [Test]
        public void ShouldReturnNoFileExists()
        {
            var actual = _fileWrapper.Exists(_path);

            Assert.That(actual, Is.False);
        }      
    }
}