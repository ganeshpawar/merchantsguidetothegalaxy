using System;
using System.IO;
using MerchantGuideToTheGalaxy.App.Core;
using MerchantGuideToTheGalaxy.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.App.Tests.ConsoleView
{
    [TestFixture]
    public class GivenAFileWithNoContent
    {
        private Mock<IFileWrapper> _fileWrapperMock;
        private MerchantGuideToTheGalaxy.App.ConsoleView _view;
        private Mock<IController> _controllerMock;
        private StringWriter _streamOut;
        private StringReader _stream;

        [SetUp]
        public void SetUp()
        {
            _stream = new StringReader("test");
            Console.SetIn(_stream);
            _streamOut = new StringWriter();
            Console.SetOut(_streamOut);
            _fileWrapperMock = new Mock<IFileWrapper>();
            _controllerMock = new Mock<IController>();
            _view = new MerchantGuideToTheGalaxy.App.ConsoleView(_fileWrapperMock.Object);
            _fileWrapperMock.Setup(fileWrapper => fileWrapper.GetFileContents(It.IsAny<string>())).Returns(String.Empty);
            _fileWrapperMock.Setup(fileWrapper => fileWrapper.Exists(It.IsAny<string>())).Returns(true);
            _controllerMock.Setup(controller => controller.Process(It.IsAny<string>()));
            _view.Initialise(_controllerMock.Object);
        }

        [Test]
        public void ViewShouldInformUser()
        {
            var actual = TestHelper.CheckStream(2, _streamOut);

            Assert.That(actual, Is.EqualTo("This file has no content in it."));
        }

        [Test]
        public void ViewShouldNotProcessFile()
        {
            _controllerMock.Verify(controller => controller.Process(It.IsAny<string>()), Times.Never());
        }

        [TearDown]
        public void Cleanup()
        {            
            _streamOut.Close();
            _streamOut = null;
            _stream.Close();
            _stream = null;
        }
    }
}