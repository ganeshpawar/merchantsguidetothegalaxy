using System;
using System.Collections.Generic;
using System.IO;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Core;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.App.Tests.ConsoleView
{
    [TestFixture]
    public class GivenTextToTranslateRatherThanAFilePath
    {
        private MerchantGuideToTheGalaxy.App.ConsoleView _view;
        private StringReader _stream;
        private StringWriter _streamOut;

        [SetUp]
        public void SetUp()
        {
            _streamOut = new StringWriter();
            _stream = new StringReader(string.Format("glob is I{0}prok is V{0}pish is X{0}tegj is L{0}glob glob Silver is 34 Credits{0}glob prok Gold is 57800 Credits{0}pish pish Iron is 3910 Credits{0}how much is pish tegj glob glob ?{0}how many Credits is glob prok Silver ?{0}how many Credits is glob prok Gold ?{0}how many Credits is glob prok Iron ?{0}how much wood could a woodchuck chuck if a woodchuck could chuck wood ?{0}", Environment.NewLine));
            Console.SetOut(_streamOut);
            var output = new List<string>();

            _view = new MerchantGuideToTheGalaxy.App.ConsoleView(new MerchantGuideToTheGalaxy.App.FileWrapper());
            var controller = new MerchantGuideToTheGalaxy.App.Controller(_view,
             new TypeResolver().Resolve<IExpressionIntepreter>(output), output);
            Console.SetIn(_stream);

            controller.Initialise();
        }

        [TestCase(25, @"pish tegj glob glob is 42")]
        [TestCase(28, @"glob prok Silver is 68 Credits")]
        [TestCase(31, @"glob prok Gold is 57800 Credits")]
        [TestCase(34, @"glob prok Iron is 782 Credits")]
        [TestCase(37, @"I have no idea what you are talking about")]
        public void ShouldTranslateText(int positionInSteam, string expected)
        {
            var actual = TestHelper.CheckStream(positionInSteam, _streamOut);
            Assert.That(actual.Trim(), Is.EqualTo(expected));
        }


        [TearDown]
        public void Cleanup()
        {
            _stream.Close();
            _stream = null;
            _streamOut.Close();
            _streamOut = null;
        }
    }
}