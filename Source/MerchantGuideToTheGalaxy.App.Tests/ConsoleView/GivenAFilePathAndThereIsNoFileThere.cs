using System;
using System.IO;
using MerchantGuideToTheGalaxy.App.Core;
using Moq;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.App.Tests.ConsoleView
{
    [TestFixture]
    public class GivenAFilePathAndThereIsNoFileThere
    {
        private Mock<IFileWrapper> _fileWrapperMock;
        private MerchantGuideToTheGalaxy.App.ConsoleView _view;
        private string _path;
        private StringReader _stream;
        private StringWriter _streamOut;

        [SetUp]
        public void SetUp()
        {
            _streamOut = new StringWriter();
            _stream = new StringReader("Test");
            Console.SetOut(_streamOut);
            Console.SetIn(_stream);

            _path = @"c:\random.txt";
            _fileWrapperMock = new Mock<IFileWrapper>();
            _fileWrapperMock.Setup(fileWrapper => fileWrapper.Exists(_path)).Returns(false);
            var controllerMock = new Mock<IController>();
            _view = new MerchantGuideToTheGalaxy.App.ConsoleView(_fileWrapperMock.Object);
            
            _view.Initialise(controllerMock.Object);
        }

        [Test]
        public void ShouldInformUserThatNoFileExists()
        {
            _fileWrapperMock.Verify(fileWrapper => fileWrapper.Exists(It.IsAny<string>()));
        }

        [Test]
        public void ShouldNotGetFileContents()
        {
            _fileWrapperMock.Verify(fileWrapper => fileWrapper.GetFileContents(_path), Times.Never);
        }

        [TearDown]
        public void Cleanup()
        {
            _stream.Close();
            _stream = null;
            _streamOut.Close();
            _streamOut = null;
        }
    }
}