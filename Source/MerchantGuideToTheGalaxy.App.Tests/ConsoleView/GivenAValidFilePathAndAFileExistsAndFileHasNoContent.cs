using System;
using System.IO;
using MerchantGuideToTheGalaxy.App.Core;
using MerchantGuideToTheGalaxy.Test.Helpers;
using Moq;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.App.Tests.ConsoleView
{
    [TestFixture]
    public class GivenAValidFilePathAndAFileExistsAndFileHasNoContent
    {
        private Mock<IFileWrapper> _fileWrapperMock;
        private MerchantGuideToTheGalaxy.App.ConsoleView _view;
        private Mock<IController> _controllerMock;
        private StringReader _stream;
        private StringWriter _streamOut;

        [SetUp]
        public void SetUp()
        {
            _streamOut = new StringWriter();
            _stream = new StringReader("Test");
            Console.SetOut(_streamOut);
            Console.SetIn(_stream);

            _fileWrapperMock = new Mock<IFileWrapper>();
            _controllerMock = new Mock<IController>();
            _view = new MerchantGuideToTheGalaxy.App.ConsoleView(_fileWrapperMock.Object);
            _fileWrapperMock.Setup(fileWrapper => fileWrapper.GetFileContents(It.IsAny<string>())).Returns(String.Empty);
            _fileWrapperMock.Setup(fileWrapper => fileWrapper.Exists(It.IsAny<string>())).Returns(true);            
            _controllerMock.Setup(controller => controller.Process(It.IsAny<string>()));
            _view.Initialise(_controllerMock.Object);
        }

        [Test]
        public void ShouldInformUser()
        {
            var actual = TestHelper.CheckStream(2, _streamOut);

            Assert.That(actual, Is.EqualTo("This file has no content in it."));
        }

        [Test]
        public void ShouldNotProcessFile()
        {
            _controllerMock.Verify(controller => controller.Process(It.IsAny<string>()), Times.Never);
        }

        [TearDown]
        public void Cleanup()
        {
            _stream.Close();
            _stream = null;
            _streamOut.Close();
            _streamOut = null;
        }
    }
}