using System.Collections.Generic;
using System.Text;
using MerchantGuideToTheGalaxy.App.Core;
using MerchantGuideToTheGalaxy.Expressions;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Core;
using Moq;
using NUnit.Framework;
using DecimalConverter = MerchantsGuideToTheGalaxy.Symbols.DecimalConverter;

namespace MerchantGuideToTheGalaxy.App.Tests.Controller
{
    [TestFixture]
    public class ControllerTests
    {
        private Mock<IView> _viewMock;
        private MerchantGuideToTheGalaxy.App.Controller _controller;
        private Dictionary<string, string> _dictionary;
        private Dictionary<string, decimal> _priceList;
        private IExpressionIntepreter _expressionIntepreter;
        private List<string> _output;

        [SetUp]
        public void Setup()
        {
            _viewMock = new Mock<IView>();
            _priceList = new Dictionary<string, decimal>();
            _priceList.Add("Silver", 17);
            _dictionary = new Dictionary<string, string>();
            _output = new List<string>();
            var decimalConverter = DecimalConverter.CreateDefault();
            var translator = new Translator(TestHelper.GetDictionary());
            var expressionTree = new List<IExpression>
            {
                new TranslationExpression(_dictionary),
                new TransactionExpression(_priceList, decimalConverter, translator),
                new QueryExpression( decimalConverter, _output, translator),
                new CreditQueryExpression(decimalConverter, _priceList, _output, translator),
                new UnknownExpression(_output)
            };
            _expressionIntepreter = new ExpressionIntepreter(expressionTree);
            _controller = new MerchantGuideToTheGalaxy.App.Controller(_viewMock.Object, _expressionIntepreter, _output);
            _controller.Initialise();
        }

        [Test]
        public void ShouldInitiliseView()
        {
            _viewMock.Verify(view => view.Initialise(_controller));
        }

        [Test]
        public void ShouldProcessInput()
        {
            var input = @"
glob is I
prok is V
pish is X
tegj is L
glob glob Silver is 34 Credits
glob prok Gold is 57800 Credits
pish pish Iron is 3910 Credits
how much is pish tegj glob glob ?
how many Credits is glob prok Silver ?
how many Credits is glob prok Gold ?
how many Credits is glob prok Iron ?
how much wood could a woodchuck chuck if a woodchuck could chuck wood ?";

            var output = @"pish tegj glob glob is 42
glob prok Silver is 68 Credits
glob prok Gold is 57800 Credits
glob prok Iron is 782 Credits
I have no idea what you are talking about";


            _controller.Process(input);

            var actual = new StringBuilder();
            foreach (var message in _output)
            {
                actual.AppendLine(message);
            }

            Assert.That(actual.ToString().Trim(), Is.EqualTo(output));
        }

        [TestCase("glob is I", "glob", "I")]
        [TestCase("prok is V", "prok", "V")]
        [TestCase("pish is X", "pish", "X")]
        [TestCase("tegj is L", "tegj", "L")]
        public void ShouldProcessSymbolMappingExpression(string input, string galactic, string romanNumeral)
        {
            _controller.Process(input);

            Assert.That(_dictionary[galactic], Is.EqualTo(romanNumeral));
        }

        [TestCase("glob glob Silver is 34 Credits", "Silver", 17)]
        [TestCase("glob prok Gold is 57800 Credits", "Gold", 14450)]
        public void ShouldProcessTransactionExpression(string input, string commodity, int price)
        {
            _controller.Process(input);

            Assert.That(_priceList[commodity], Is.EqualTo(price));
        }


        [TestCase("how much is pish tegj glob glob ?", "pish tegj glob glob is 42")]
        [TestCase("how many Credits is glob prok Silver ?", "glob prok Silver is 68 Credits")]
        [TestCase("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?", "I have no idea what you are talking about")]
        public void ShouldProcessQueryExpression(string query, string answer)
        {
            _controller.Process(query);

            Assert.That(_output[0], Is.EqualTo(answer));
        }
    }
}