using System.Collections.Generic;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.CreditQueryExpression
{
    [TestFixture(Category = "CreditQueryExpression")]
    public class GivenAPriceAndAllTranslation
    {
        [TestCase("how many Credits is glob prok Silver ?", "glob prok Silver is 68 Credits")]
        public void ShouldReturnAnAnswer(string query, string answer)
        {
            var output = new List<string>();
            var converter = DecimalConverter.CreateDefault();
            Dictionary<string, decimal> priceList = new Dictionary<string, decimal> { { "Silver", 17 } };
            var translator = new Expressions.Translator(TestHelper.GetDictionary());
            var creditQueryExpression = new Expressions.CreditQueryExpression(converter, priceList, output, translator);
            creditQueryExpression.Process(query);

            Assert.That(output[0], Is.EqualTo(answer));
        }
    }
}