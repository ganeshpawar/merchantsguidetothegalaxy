using System.Collections.Generic;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.TransactionExpression
{
    [TestFixture]
    public class GivenTheSameCommodityRepeatedTwiceWithDifferentPrices
    {
        [Test]
        public void ShouldUpdateThePriceListWithTheRecentExpression()
        {
            var priceList = new Dictionary<string, decimal>();
            var converter = DecimalConverter.CreateDefault();
            var expression = "glob glob Silver is 34 Credits";
            var expressionTwo = "glob glob Silver is 38 Credits";
            var translator = new Expressions.Translator(TestHelper.GetDictionary());
            var translationExpression = new Expressions.TransactionExpression(priceList, converter, translator);
            translationExpression.Process(expression);
            translationExpression.Process(expressionTwo);

            Assert.That(priceList["Silver"], Is.EqualTo(19));
        }
    }
}