using System;
using System.Collections.Generic;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Core;
using Moq;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.TransactionExpression
{
    [TestFixture]
    public class GivenAnyOtherError
    {
        [Test]
        public void ShouldReturnFalse()
        {
            var priceList = new Dictionary<string, decimal>();
            var mockConverter = new Mock<IDecimalConverter>();
            mockConverter.Setup(converter => converter.Convert(It.IsAny<string>())).Throws<FormatException>();
            var expression = "glob glob Silver is 34 Credits";
            var translator = new Expressions.Translator(TestHelper.GetDictionary());
            var translationExpression = new Expressions.TransactionExpression(priceList, mockConverter.Object, translator);
            var actual = translationExpression.Process(expression);

            Assert.That(actual, Is.False);
        }
    }
}