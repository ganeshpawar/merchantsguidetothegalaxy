using System.Collections.Generic;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.TransactionExpression
{
    [TestFixture]
    public class GivenAllTranslationsAndADecimalConverter
    {
        [Test]
        public void ShouldUpdatePriceList()
        {
            var priceList = new Dictionary<string, decimal>();
            var converter = DecimalConverter.CreateDefault();
            var expression = "glob glob Silver is 34 Credits";
            var translator = new Expressions.Translator(TestHelper.GetDictionary());
            var translationExpression = new Expressions.TransactionExpression(priceList, converter, translator);
            translationExpression.Process(expression);

            Assert.That(priceList["Silver"], Is.EqualTo(17));
        }
    }
}