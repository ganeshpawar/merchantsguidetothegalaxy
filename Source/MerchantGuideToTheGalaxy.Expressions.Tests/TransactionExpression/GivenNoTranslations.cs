using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.TransactionExpression
{
    [TestFixture]
    public class GivenNoTranslations
    {
        [Test]
        public void ShouldReturnFalse()
        {
            var priceList = new Dictionary<string, decimal>();
            var converter = DecimalConverter.CreateDefault();
            var expression = "glob glob Silver is 34 Credits";
            var translator = new Expressions.Translator(new Dictionary<string, string>());
            var translationExpression = new Expressions.TransactionExpression(priceList, converter, translator);
            var actual = translationExpression.Process(expression);

            Assert.That(actual, Is.False);
        }
    }
}