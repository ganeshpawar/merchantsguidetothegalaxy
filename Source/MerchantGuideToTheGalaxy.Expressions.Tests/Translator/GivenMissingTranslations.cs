using System.Collections.Generic;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.Translator
{
    [TestFixture]
    public class GivenMissingTranslations
    {
        [Test]
        public void ShouldThrowException()
        {
            var dictionary = new Dictionary<string,string>();
            dictionary.Add("glob", "I");
            var translator = new Expressions.Translator(dictionary);
         
            Assert.Throws<KeyNotFoundException>(() => translator.Translate("glob prok"));
        }
    }
}