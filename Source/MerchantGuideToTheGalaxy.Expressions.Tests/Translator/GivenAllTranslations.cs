using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.Translator
{
    [TestFixture]
    public class GivenAllTranslations
    {
        [Test]
        public void ShouldConvertGalaticToRomanNumerals()
        {
            var dictionary = new Dictionary<string,string>();
            dictionary.Add("glob", "I");
            var translator = new Expressions.Translator(dictionary);
            var translation = translator.Translate("glob glob");

            Assert.That(translation, Is.EqualTo("I I"));
        }
    }
}