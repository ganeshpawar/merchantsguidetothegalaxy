using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;
using Moq;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests
{
    [TestFixture]
    public class ExpressionIntepreterTests
    {
        [Test]
        public void ShouldCallExpressionMatchFunction()
        {
            var mockExpression = new Mock<IExpression>();
            mockExpression.Setup(expression => expression.MatchesFormat(@"test")).Returns(true);
            var expressionTree = new List<IExpression> { mockExpression.Object };
            var factory = new ExpressionIntepreter(expressionTree);
            factory.ProcessExpressions(@"test");

            mockExpression.Verify(expression => expression.MatchesFormat(It.IsAny<string>()));
        }

        [Test]
        public void ShouldCallExpressionProcess()
        {
            var testExpression = @"test";
            var mockExpression = new Mock<IExpression>();
            mockExpression.Setup(expression => expression.MatchesFormat(testExpression)).Returns(true);
            mockExpression.Setup(expression => expression.Process(testExpression));
            var expressionTree = new List<IExpression> { mockExpression.Object };
            var factory = new ExpressionIntepreter(expressionTree);
            factory.ProcessExpressions(testExpression);

            mockExpression.Verify(expression => expression.Process(It.IsAny<string>()));
        }  
        
        [Test]
        public void ShouldTryNextExpressionWhenCurrentExpressionFailedToProcess()
        {
            var testExpression = @"test";
            var mockExpression = new Mock<IExpression>();
            var mockExpressionTwo = new Mock<IExpression>();
            mockExpression.Setup(expression => expression.MatchesFormat(testExpression)).Returns(true);
            mockExpression.Setup(expression => expression.Process(testExpression)).Returns(false);
            mockExpressionTwo.Setup(expression => expression.MatchesFormat(testExpression)).Returns(true);
            mockExpressionTwo.Setup(expression => expression.Process(testExpression)).Returns(false);
            var expressionTree = new List<IExpression> { mockExpression.Object, mockExpressionTwo.Object };
            var factory = new ExpressionIntepreter(expressionTree);
            factory.ProcessExpressions(testExpression);

            mockExpressionTwo.Verify(expression => expression.Process(It.IsAny<string>()));
        }  
        
        [Test]
        public void ShouldNotTryNextExpressionWhenCurrentExpressionSuccessullyToProcessesString()
        {
            var testExpression = @"test";
            var mockExpression = new Mock<IExpression>();
            var mockExpressionTwo = new Mock<IExpression>();
            mockExpression.Setup(expression => expression.MatchesFormat(testExpression)).Returns(true);
            mockExpression.Setup(expression => expression.Process(testExpression)).Returns(true);
            mockExpressionTwo.Setup(expression => expression.MatchesFormat(testExpression));
            mockExpressionTwo.Setup(expression => expression.Process(testExpression));
            var expressionTree = new List<IExpression> { mockExpression.Object, mockExpressionTwo.Object };
            var factory = new ExpressionIntepreter(expressionTree);
            factory.ProcessExpressions(testExpression);

            mockExpressionTwo.Verify(expression => expression.MatchesFormat(testExpression), Times.Never);
            mockExpressionTwo.Verify(expression => expression.Process(It.IsAny<string>()), Times.Never);
        }       
    }
}