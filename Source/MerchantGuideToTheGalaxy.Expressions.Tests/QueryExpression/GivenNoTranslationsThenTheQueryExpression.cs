using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.QueryExpression
{
    [TestFixture(Category = "QueryExpression")]
    public class GivenNoTranslationsThenTheQueryExpression
    {
        [Test]
        public void ShouldReturnFalse()
        {
            var converter = DecimalConverter.CreateDefault();
            var translator = new Expressions.Translator(new Dictionary<string, string>());
            var output = new List<string>();
            var queryExpression = new Expressions.QueryExpression(converter, output, translator);
            var actual = queryExpression.Process("how much is pish tegj glob glob ?");

            Assert.That(actual, Is.False);
        }
    }
}