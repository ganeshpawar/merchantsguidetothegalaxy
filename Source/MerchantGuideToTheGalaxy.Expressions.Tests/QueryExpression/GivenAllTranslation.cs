using System.Collections.Generic;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Core;
using MerchantsGuideToTheGalaxy.Symbols;
using Moq;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.QueryExpression
{
    [TestFixture(Category = "QueryExpression")]
    public class GivenAllTranslation
    {
        [TestCase("how much is pish tegj glob glob ?", "pish tegj glob glob is 42")]
        public void ShouldAnswerQuery(string query, string answer)
        {
            var converter = DecimalConverter.CreateDefault();
            var translator = new Expressions.Translator(TestHelper.GetDictionary());
            var output = new List<string>();
            var queryExpression = new Expressions.QueryExpression(converter, output, translator);
            queryExpression.Process(query);

            Assert.That(output[0], Is.EqualTo(answer));
        }

        [Test()]
        public void ShouldCallConvertMethod()
        {
            var converterMock = new Mock<IDecimalConverter>();
            converterMock.Setup(converter => converter.Convert(It.IsAny<string>()));
            var output = new List<string>();
            var translator = new Expressions.Translator(TestHelper.GetDictionary());
            var queryExpression = new Expressions.QueryExpression(converterMock.Object, output, translator);
            queryExpression.Process("");

            converterMock.Verify(converter => converter.Convert(It.IsAny<string>()));
        }
    }
}