using System.Collections.Generic;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.QueryExpression
{
    [TestFixture(Category = "QueryExpression")]
    public class GivenAQueryStringExpression
    {
        [Test]
        public void ShouldMatchOnTheWordsHowMuchIs()
        {
            var converter = DecimalConverter.CreateDefault();
            var translator = new Expressions.Translator(TestHelper.GetDictionary());
            var output = new List<string>();
            var queryExpression = new Expressions.QueryExpression(converter, output, translator);
            var actual = queryExpression.MatchesFormat("how much is pish tegj glob glob ?");

            Assert.That(actual, Is.True);
        }
    }
}