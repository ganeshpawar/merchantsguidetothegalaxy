using System.Collections.Generic;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.UnknownExpression
{
    [TestFixture]
    public class GivenAnyStringExpression
    {
        private List<string> _output;
        private Expressions.UnknownExpression _expression;

        [SetUp]
        public void SetUp()
        {
            _output = new List<string>();
            _expression = new Expressions.UnknownExpression(_output);
        }

        [Test]
        public void ShouldMatchOnAnyString()
        {
            var actual = _expression.MatchesFormat("gobble gook");

            Assert.That(actual, Is.True);
        }

        [Test]
        public void ShouldAddToOutput()
        {
            _expression.Process("gobble gook");

            Assert.That(_output[0], Is.EqualTo("I have no idea what you are talking about"));
        }
    }
}