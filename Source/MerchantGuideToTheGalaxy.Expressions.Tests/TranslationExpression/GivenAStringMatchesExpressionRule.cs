using System.Collections.Generic;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.TranslationExpression
{
    [TestFixture()]
    public class GivenAStringMatchesExpressionRule
    {
        [Test]
        public void ShouldAddWordToDictionary()
        {
            var expression = "glob is I";
            var dictionary = new Dictionary<string, string>();
            var translationExpression = new Expressions.TranslationExpression(dictionary);
            translationExpression.MatchesFormat(expression);
            translationExpression.Process(expression);

            Assert.That(dictionary["glob"], Is.EqualTo("I"));
        }
    }
}