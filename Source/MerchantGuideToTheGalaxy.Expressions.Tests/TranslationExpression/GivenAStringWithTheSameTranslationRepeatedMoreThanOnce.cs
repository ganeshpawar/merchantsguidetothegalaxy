using System.Collections.Generic;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.TranslationExpression
{
    [TestFixture()]
    public class GivenAStringWithTheSameTranslationRepeatedMoreThanOnce
    {
        [Test]
        public void ShouldAddTranslationOnce()
        {
            var expression = "glob is I";
            var dictionary = new Dictionary<string, string>();
            var translationExpression = new Expressions.TranslationExpression(dictionary);
            translationExpression.Process(expression);
            translationExpression.Process(expression);

            Assert.That(dictionary["glob"], Is.EqualTo("I"));
        }
    }
}