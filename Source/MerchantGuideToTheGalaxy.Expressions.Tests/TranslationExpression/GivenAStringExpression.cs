using System.Collections.Generic;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.TranslationExpression
{
    [TestFixture()]
    public class GivenAStringExpression
    {
        [Test]
        public void ShouldMatchBasedOnWordCountEqualThree()
        {
            var expression = "glob is I";
            var dictionary = new Dictionary<string, string>();
            var translationExpression = new Expressions.TranslationExpression(dictionary);
            var actual = translationExpression.MatchesFormat(expression);

            Assert.That(actual, Is.True);
        }
    }
}