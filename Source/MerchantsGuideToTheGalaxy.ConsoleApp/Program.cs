﻿using System.Collections.Generic;
using MerchantGuideToTheGalaxy.App;
using MerchantGuideToTheGalaxy.App.Core;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantsGuideToTheGalaxy.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var output = new List<string>();
            var controller = new Controller(new ConsoleView(new FileWrapper()),
                new TypeResolver().Resolve<IExpressionIntepreter>(output), output);
            var app = new App(controller);            
            
            app.Initialise();
        }
    }
}
