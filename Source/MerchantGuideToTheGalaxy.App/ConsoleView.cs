using System;
using System.IO;
using MerchantGuideToTheGalaxy.App.Core;

namespace MerchantGuideToTheGalaxy.App
{
    public class ConsoleView : IView
    {
        private readonly IFileWrapper _fileWrapper;

        public ConsoleView(IFileWrapper fileWrapper)
        {
            _fileWrapper = fileWrapper;            
        }

        public void DisplayMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(message + CharacterToAllowTesting);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public const string CharacterToAllowTesting = ">";

        public void Initialise(IController controller)
        {
            PrintWelcomeMessageInstructions();
            var input = Console.ReadLine();
            while (input != null)
            {
                if (ProcessExitCommand(input)) break;
                
                if (!_fileWrapper.Exists(input))
                {
                    TryProcessAsText(controller, input);
                }
                else
                {
                    TryProcessAsFile(controller, input);
                }
                input = Console.ReadLine();
            }
            
        }

        private void TryProcessAsFile(IController controller, string input)
        {
            var content = _fileWrapper.GetFileContents(input);
            if (!string.IsNullOrEmpty(content))
            {
                Console.WriteLine($@"Processing {input}. {CharacterToAllowTesting}");
                var result = controller.Process(content);
                DisplayMessage(result);
            }
            else
            {
                Console.WriteLine($"This file has no content in it. {CharacterToAllowTesting}");
            }
        }

        private void TryProcessAsText(IController controller, string input)
        {
            Console.WriteLine(
                $@"You have entered an invalid path or no file exists at that location {input}. Attempting to process as standard text {CharacterToAllowTesting}");
            if (!string.IsNullOrEmpty(input))
            {
                Console.WriteLine($@"Processing {input}. {CharacterToAllowTesting}");
                var result = controller.Process(input);
                DisplayMessage(result);
            }
            else
            {
                Console.WriteLine($"No information has been entered, please try again. {CharacterToAllowTesting}");
            }
        }

        private static bool ProcessExitCommand(string input)
        {
            if (input.Equals("exit"))
            {
                Console.WriteLine("Shutting down application. Please press enter to close the console.");
                Console.ReadLine();
                return true;
            }
            return false;
        }

        private static void PrintWelcomeMessageInstructions()
        {
            Console.WriteLine("Welcome to Merchant Guide To Galaxy." + CharacterToAllowTesting);
            Console.WriteLine("Please enter a file path or text and press enter. Type exit to leave." + CharacterToAllowTesting);
        }
    }
}