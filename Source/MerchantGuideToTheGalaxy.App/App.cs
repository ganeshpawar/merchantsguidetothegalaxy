using MerchantGuideToTheGalaxy.App.Core;

namespace MerchantGuideToTheGalaxy.App
{
    public class App : IApp
    {
        private readonly IController _controller;

        public App(IController controller)
        {
            _controller = controller;
        }

        public void Initialise()
        {
            _controller.Initialise();
        }       
    }
}