using System.IO;
using MerchantGuideToTheGalaxy.App.Core;

namespace MerchantGuideToTheGalaxy.App
{
    public class FileWrapper : IFileWrapper
    {
        public string GetFileContents(string path)
        {
            return File.ReadAllText(path);
        }

        public bool Exists(string path)
        {
            return File.Exists(path);
        }
    }
}