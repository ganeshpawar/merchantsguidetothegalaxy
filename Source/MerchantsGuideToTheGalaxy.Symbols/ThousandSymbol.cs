using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantsGuideToTheGalaxy.Symbols
{
    public class ThousandSymbol : SubtractableSymbol
    {
        public ThousandSymbol() : base("M", 1000, new List<Symbol>())
        {

        }
    }
}