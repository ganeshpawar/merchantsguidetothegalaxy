using MerchantsGuideToTheGalaxy.Core;

namespace MerchantsGuideToTheGalaxy.Symbols
{
    public class FiftySymbol :Symbol
    {
        public FiftySymbol():base("L", 50) { }
        public FiftySymbol(string identifier):base(identifier, 50) { }
    }
}