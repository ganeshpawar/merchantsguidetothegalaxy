using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantsGuideToTheGalaxy.Symbols
{
    public class DecimalConverter : IDecimalConverter
    {
        private readonly List<Symbol> _expressionTree;

        public DecimalConverter(List<Symbol> expressionTree)
        {
            _expressionTree = expressionTree;
        }

        public int Convert(string romanNumerals)
        {
            int v = 0;
            foreach (var expression in _expressionTree)
            {
                v += expression.Convert(romanNumerals) ?? 0;
            }

            return v;
        }


        public static IDecimalConverter CreateDefault()
        {
            var expressionTree = new List<Symbol>()
            {
                new ThousandSymbol(),
                new FiveHundredSymbol(),
                new HundredSymbol(),
                new FiftySymbol(),
                new TenSymbol(),
                new FiveSymbol(),
                new OneSymbol()
            };
            return new DecimalConverter(expressionTree);
        }
    }
}