using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantsGuideToTheGalaxy.Symbols
{
    public class TenSymbol : SubtractableSymbol
    {
        public TenSymbol() : base("X", 10, new List<Symbol> {new FiftySymbol(), new HundredSymbol()}) { }
        public TenSymbol(string identifier, List<Symbol> subtractable) : base(identifier, 10, subtractable) { }
    }
}