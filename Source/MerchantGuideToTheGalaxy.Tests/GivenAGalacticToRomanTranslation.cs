using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Symbols.Tests
{
    [TestFixture]
    public class GivenAGalacticToRomanTranslation
    {
        [TestCase("glob", 1)]
        [TestCase("glob glob", 2)]
        [TestCase("prok", 5)]
        [TestCase("glob prok", 4)]
        [TestCase("prok glob", 6)]
        [TestCase("pish", 10)]
        [TestCase("pish pish pish glob pish", 39)]
        [TestCase("tegj", 50)]
        [TestCase("tegj pish", 60)]
        public void ShouldConvertGalacticToDecimal(string galactic, int decimalValue)
        {
            var tegj = new FiftySymbol("tegj");
            var pish = new TenSymbol("pish", new List<Symbol> { tegj });
            var prok = new FiveSymbol("prok");
            var glob = new OneSymbol("glob", new List<Symbol> { prok, pish });

            var expressionTree = new List<Symbol>()
            {
                tegj,
                pish,
                prok,
                glob
            };
            var converter = new DecimalConverter(expressionTree);
            var actual = converter.Convert(galactic);

            Assert.That(actual, Is.EqualTo(decimalValue));
        }
    }
}