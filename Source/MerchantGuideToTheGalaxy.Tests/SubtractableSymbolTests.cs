﻿using System;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Symbols.Tests
{
    [TestFixture]
    public class SubtractableSymbolTests
    {
        private OneSymbol _symbol;

        [SetUp]
        public void SetUp()
        {
            _symbol = new OneSymbol();
        }

        [Test]
        public void ShouldReturn1IfOnlySymbol()
        {
            var actual = _symbol.Convert("I");

            Assert.That(actual, Is.EqualTo(1));
        }

        [Test]
        public void ShouldAdd1WhenSymbolIsAfterItSelf()
        {
            var actual = _symbol.Convert("I I");

            Assert.That(actual, Is.EqualTo(2));
        }

        [Test]
        public void ShouldAdd1WhenSymbolIsAfterSymbolGreateThanItself()        
        {
            var actual = _symbol.Convert("V I");

            Assert.That(actual, Is.EqualTo(1));
        }

        [Test]
        public void ShouldAdd1WhenSymbolIsAfterSymbolGreateThanItselfAndItself()
        {
            var actual = _symbol.Convert("V I I");

            Assert.That(actual, Is.EqualTo(2));
        }


        [TestCase("V I I I I")]
        [TestCase("X I I I I")]
        [TestCase("I I I I")]
        public void ShouldThrowFormatException(string numeral)
        {
            Assert.Throws<FormatException>(() => _symbol.Convert(numeral));
        }

        [Test]
        public void ShouldMinus1WhenSymbolIsBeforeSymbolGreateThanItself()
        {
            var actual = _symbol.Convert("I V");

            Assert.That(actual, Is.EqualTo(-1));
        }

        [TestCase("I I V")]
        [TestCase("I I X")]
        public void ShouldThrowFormatExceptionWhenSymbolRepeatedTwiceBeforeGreaterThanSymbol(string numeral)
        {
            Assert.Throws<FormatException>(() => _symbol.Convert(numeral));
        }
    }
}
