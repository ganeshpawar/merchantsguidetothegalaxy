using System;
using MerchantsGuideToTheGalaxy.Core;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Symbols.Tests
{
    [TestFixture]
    public class DecimalConverterTest
    {
        private IDecimalConverter _converter;

        [SetUp]
        public void Setup()
        {
            _converter = DecimalConverter.CreateDefault();
        }

        [TestCase("I", 1)]
        [TestCase("I I", 2)]
        [TestCase("I I I", 3)]
        [TestCase("V", 5)]
        [TestCase("V I", 6)]
        [TestCase("V I I", 7)]
        [TestCase("X", 10)]
        [TestCase("X X X I X", 39)]
        [TestCase("L", 50)]
        [TestCase("L X", 60)]
        [TestCase("C", 100)]
        [TestCase("C X", 110)]
        [TestCase("D", 500)]
        [TestCase("D C", 600)]
        [TestCase("M", 1000)]
        [TestCase("M C", 1100)]
        [TestCase("M C M I I I", 1903)]
        public void ShouldConvertRomanNumeralsToDecimal(string romanNumerals, int decimalValue)
        {
            var actual = _converter.Convert(romanNumerals);

            Assert.That(actual, Is.EqualTo(decimalValue));
        }

        [TestCase("M M M M")]
        [TestCase("I I I I")] 
        [TestCase("X X X X")]
        public void ShouldThrowFormatExceptionGivenFourConsecutiveRepitionsOfSymbol(string symbol)
        {
            Assert.Throws<FormatException>(() => _converter.Convert(symbol));
        }

        [TestCase("I V", 4)]
        [TestCase("I X", 9)]     
        public void ShouldSubtractOneWhenIIsInfrontOfVorXOnly(string symbol, int value)
        {
            var actual = _converter.Convert(symbol);

            Assert.That(actual, Is.EqualTo(value));
        }

        [TestCase("X L", 40)]
        [TestCase("X C", 90)]
        public void ShouldSubtractTenWhenXIsInfrontOfLorCOnly(string symbol, int value)
        {
            var actual = _converter.Convert(symbol);
            
            Assert.That(actual, Is.EqualTo(value));
        }
        
        [TestCase("C D", 400)]
        [TestCase("C M", 900)]
        public void ShouldSubtract100WhenCIsInfrontOfDorMOnly(string symbol, int value)
        {           
            var actual = _converter.Convert(symbol);
            
            Assert.That(actual, Is.EqualTo(value));
        }

        [TestCase("I I V")]
        [TestCase("I I X")]
        [TestCase("X X L")]
        [TestCase("X X C")]
        [TestCase("C C D")]
        [TestCase("C C M")]
        public void ShouldOnlySubtractOnlyOneSmallValueSymbolFromALargeValueSymbol(string romanNumeral)
        {
            Assert.Throws<FormatException>(() => _converter.Convert(romanNumeral));
        }
    }
}