﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using MerchantGuideToTheGalaxy.App;
using MerchantGuideToTheGalaxy.Web.Models;
using MerchantsGuideToTheGalaxy.Core;
using Controller = System.Web.Mvc.Controller;

namespace MerchantGuideToTheGalaxy.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly App.Controller _controller;

        public HomeController() 
        {
            List<string> output = new List<string>();
            var expressionIntepreter = new TypeResolver().Resolve<IExpressionIntepreter>(output);
            _controller = new MerchantGuideToTheGalaxy.App.Controller(expressionIntepreter, output);
        }

        public ActionResult Index()
        {
            return View(new HomeModel());
        }
      
        public ViewResult ProcessFile(HttpPostedFileBase file)
        {
            if (file != null)
            {
                var contentToProcess = new StreamReader(file.InputStream).ReadToEnd();
                return Process(contentToProcess);
            }
            else
            {
                return View("Index", new HomeModel
                {
                    Errors = "Failed to upload file"
                });
            }
        }

        public ViewResult Process(string content)
        {
            var result = string.Empty;
            var errors = string.Empty;

            if (!string.IsNullOrEmpty(content))
            {
                result = _controller.Process(content);
            }
            else
            {
                errors = "No content.";
            }

            return View("Index", new HomeModel
            {
                Result = result.Trim(),
                Errors = errors
            });
        }
       
    }
}