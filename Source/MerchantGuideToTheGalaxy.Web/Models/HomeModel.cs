﻿namespace MerchantGuideToTheGalaxy.Web.Models
{
    public class HomeModel
    {
        public string Result { get; set; }
        public string Errors { get; set; }
    }
}