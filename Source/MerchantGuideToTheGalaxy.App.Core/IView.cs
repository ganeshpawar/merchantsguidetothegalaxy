namespace MerchantGuideToTheGalaxy.App.Core
{
    public interface IView
    {
        void DisplayMessage(string message);
        void Initialise(IController controller);
    }
}