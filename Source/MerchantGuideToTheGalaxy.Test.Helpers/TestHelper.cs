using System;
using System.Collections.Generic;
using System.IO;

namespace MerchantGuideToTheGalaxy.Test.Helpers
{
    public static class TestHelper
    {
        public static Dictionary<string, string> GetDictionary()
        {
            return new Dictionary<string, string>
            {
                {"glob", "I"}, {"prok", "V"}, {"pish", "X"}, {"tegj", "L"}
            };
        }

        public static string CheckStream(int position, StringWriter streamOut)
        {
            const string splitCharacter = ">";
            return streamOut.ToString()
                .Trim()
                .Split(splitCharacter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[position]
                .Trim();
        }
    }
}