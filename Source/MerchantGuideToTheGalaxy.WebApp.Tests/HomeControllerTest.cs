﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MerchantGuideToTheGalaxy.Web.Controllers;
using MerchantGuideToTheGalaxy.Web.Models;
using Moq;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.WebApp.Tests
{
    [TestFixture]
    public class HomeControllerTest
    {
        [Test]
        public void ShouldProcessFile()
        {
            var input = @"
glob is I
prok is V
pish is X
tegj is L
glob glob Silver is 34 Credits
glob prok Gold is 57800 Credits
pish pish Iron is 3910 Credits
how much is pish tegj glob glob ?
how many Credits is glob prok Silver ?
how many Credits is glob prok Gold ?
how many Credits is glob prok Iron ?
how much wood could a woodchuck chuck if a woodchuck could chuck wood ?";
            var expected = @"pish tegj glob glob is 42
glob prok Silver is 68 Credits
glob prok Gold is 57800 Credits
glob prok Iron is 782 Credits
I have no idea what you are talking about";
            using (var stream = GenerateStreamFromString(input))
            {
                var fileMock = new Mock<HttpPostedFileBase>();
                fileMock.Setup(file => file.ContentLength).Returns(input.Length);
                fileMock.Setup(file => file.InputStream).Returns(stream);
                var homeController = new HomeController();
                var actionResult = homeController.ProcessFile(fileMock.Object) as ViewResult;
                var model = actionResult.Model as HomeModel;

                Assert.That(model.Result.Trim(), Is.EqualTo(expected));
            }
        }
        
        [Test]
        public void ShouldWarnAboutEmptyFileContents()
        {
            var input = @"";
            var expected = @"";
            using (var stream = GenerateStreamFromString(input))
            {
                var fileMock = new Mock<HttpPostedFileBase>();
                fileMock.Setup(file => file.ContentLength).Returns(input.Length);
                fileMock.Setup(file => file.InputStream).Returns(stream);
                var homeController = new HomeController();
                var actionResult = homeController.ProcessFile(fileMock.Object) as ViewResult;
                var model = actionResult.Model as HomeModel;

                Assert.That(model.Errors, Is.EqualTo("No content."));
            }
        } 
        
        [Test]
        public void ShouldProcessText()
        {
            var input = @"
glob is I
prok is V
pish is X
tegj is L
glob glob Silver is 34 Credits
glob prok Gold is 57800 Credits
pish pish Iron is 3910 Credits
how much is pish tegj glob glob ?
how many Credits is glob prok Silver ?
how many Credits is glob prok Gold ?
how many Credits is glob prok Iron ?
how much wood could a woodchuck chuck if a woodchuck could chuck wood ?";
            var expected = @"pish tegj glob glob is 42
glob prok Silver is 68 Credits
glob prok Gold is 57800 Credits
glob prok Iron is 782 Credits
I have no idea what you are talking about";
            
                var homeController = new HomeController();
                var actionResult = homeController.Process(input) as ViewResult;
                var model = actionResult.Model as HomeModel;

                Assert.That(model.Result.Trim(), Is.EqualTo(expected));
        }

        [Test]
        public void ShouldWarnAboutEmptyTextContents()
        {
            var homeController = new HomeController();
            var actionResult = homeController.Process(string.Empty) as ViewResult;
            var model = actionResult.Model as HomeModel;

            Assert.That(model.Errors, Is.EqualTo("No content."));
        }



        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }

  
}
