namespace MerchantsGuideToTheGalaxy.Core
{
    public interface IExpression
    {
        bool MatchesFormat(string expression);
        bool Process(string expression);
    }
}