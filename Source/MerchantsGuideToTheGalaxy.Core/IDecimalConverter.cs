namespace MerchantsGuideToTheGalaxy.Core
{
    public interface IDecimalConverter
    {
        int Convert(string romanNumerals);
    }
}