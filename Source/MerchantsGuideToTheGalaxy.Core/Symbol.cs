namespace MerchantsGuideToTheGalaxy.Core
{
    public abstract class Symbol
    {
        protected int Value;

        public string Identifer { get; set; }

        protected Symbol(string identifier, int value)
        {
            Identifer = identifier;
            Value = value;
        }

        public  virtual int? Convert(string romanNumerals)
        {
            if (romanNumerals.Contains(Identifer))
            {
                return Value;
            }
            return null;
        }
    }
}