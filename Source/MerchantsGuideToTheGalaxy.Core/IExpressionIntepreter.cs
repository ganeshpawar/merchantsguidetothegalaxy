namespace MerchantsGuideToTheGalaxy.Core
{
    public interface IExpressionIntepreter
    {
        void ProcessExpressions(string expressions);
    }
}