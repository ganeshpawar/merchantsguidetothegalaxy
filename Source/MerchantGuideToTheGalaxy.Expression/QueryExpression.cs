using System;
using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantGuideToTheGalaxy.Expressions
{
    public class QueryExpression : IExpression
    {
        private readonly IDecimalConverter _converter;
        private readonly List<string> _output;
        private readonly ITranslator _translator;

        public QueryExpression(IDecimalConverter converter, List<string> output, ITranslator translator)
        {
              _converter = converter;
            _output = output;
            _translator = translator;
        }

        public bool MatchesFormat(string expression)
        {
            return expression.StartsWith("how much is");
        }

        public bool Process(string expression)
        {
            try
            {
                var strQuantity = expression.Replace("how much is ", String.Empty)
                    .Replace("?", String.Empty)
                    .Trim();
                var quantity = _converter.Convert(_translator.Translate(strQuantity));
                _output.Add($"{strQuantity} is {quantity}");
                return true;
            }
            catch (Exception error)
            {
                //log error
                return false;
            }
        }
    }
}