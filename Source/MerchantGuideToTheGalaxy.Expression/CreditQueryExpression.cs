using System;
using System.Collections.Generic;
using System.Linq;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantGuideToTheGalaxy.Expressions
{
    public class CreditQueryExpression : IExpression
    {
        private IDecimalConverter _converter;
        private Dictionary<string,decimal> _priceList;
        private List<string> _output;
        private readonly ITranslator _translator;

        public CreditQueryExpression(IDecimalConverter converter, 
            Dictionary<string, decimal> priceList, 
            List<string> output, 
            ITranslator translator)
        {
            _converter = converter;
            _priceList = priceList;
            _output = output;
            _translator = translator;
        }

        public bool MatchesFormat(string expression)
        {
            return expression.StartsWith("how many Credits is");
        }

        public bool Process(string expression)
        {
            try
            {
                expression = expression.Replace("how many Credits is ", string.Empty)
                    .Replace("?", string.Empty)
                    .Trim();

                var expressionComponents = expression.Split(" ".ToCharArray(),
                    StringSplitOptions.RemoveEmptyEntries).ToList();

                var commodity = expressionComponents[expressionComponents.Count - 1];
                if (!_priceList.ContainsKey(commodity))
                {
                    return false;
                }
                var strQuantity = expression.Replace(commodity, string.Empty)
                    .Trim();

                var translation = _translator.Translate(strQuantity);
                var quantity = _converter.Convert(translation);
                var totalCredits = Math.Truncate(quantity*_priceList[commodity]);

                _output.Add($"{strQuantity} {commodity} is {totalCredits} Credits");

                return true;
            }
            catch (Exception error)
            {
                //log error
                return false;
            }
        }
    }
}