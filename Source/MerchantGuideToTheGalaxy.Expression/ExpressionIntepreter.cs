using System;
using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantGuideToTheGalaxy.Expressions
{
    public class ExpressionIntepreter : IExpressionIntepreter
    {
        private const string Delimiter = "\n\r";
        private readonly List<IExpression> _expressionTree;

        public ExpressionIntepreter(List<IExpression> expressionTree)
        {
            _expressionTree = expressionTree;
        }

        public void ProcessExpressions(string expressions)
        {
            var lines = expressions.Split(Delimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var expressionString in lines)
            {
                foreach (var expression in _expressionTree)
                {
                    if (expression.MatchesFormat(expressionString))
                    {
                        var successful = expression.Process(expressionString);
                        if (successful)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }
}