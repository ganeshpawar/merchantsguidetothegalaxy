﻿using System;
using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantGuideToTheGalaxy.Expressions
{
    public class TransactionExpression : IExpression
    {
        private const string Delimiter = " ";
        private const int TotalsCreditPosition = 2;
        private readonly Dictionary<string, decimal> _priceList;
        private readonly IDecimalConverter _converter;
        private readonly ITranslator _translator;
        private const int CommodityNamePosition = 4;

        public TransactionExpression(Dictionary<string, decimal> priceList, IDecimalConverter converter,
            ITranslator translator)
        {
            _priceList = priceList;
            _converter = converter;
            _translator = translator;
            
        }

        public bool MatchesFormat(string expression)
        {
            return expression.EndsWith("Credits");
        }

        public bool Process(string expression)
        {
            try
            {
                var elements = expression.Split(Delimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                var commodityName = elements[elements.Length - CommodityNamePosition];
                var totalCredits = int.Parse(elements[elements.Length - TotalsCreditPosition]);
                var strQuantity =
                    expression.Remove(expression.IndexOf(commodityName, StringComparison.InvariantCultureIgnoreCase))
                        .Trim();
                var quantity = _converter.Convert(_translator.Translate(strQuantity));
                var price = Decimal.Divide(totalCredits, quantity);

                if (!_priceList.ContainsKey(commodityName))
                {
                    _priceList.Add(commodityName, price);
                }
                else
                {
                    _priceList[commodityName] =  price;
                }

                return true;
            }
            catch (Exception error)
            {
                return false;
            }
        }
    }
}
