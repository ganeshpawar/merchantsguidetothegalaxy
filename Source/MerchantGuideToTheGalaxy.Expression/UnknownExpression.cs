using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantGuideToTheGalaxy.Expressions
{
    public class UnknownExpression : IExpression
    {
        private readonly List<string> _output;

        public UnknownExpression(List<string> output)
        {
            _output = output;
        }

        public bool MatchesFormat(string expression)
        {
            return true;//match any expression
        }

        public bool Process(string expression)
        {
            _output.Add("I have no idea what you are talking about");
            return true;
        }
    }
}