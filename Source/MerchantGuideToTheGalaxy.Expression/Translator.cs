using System;
using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantGuideToTheGalaxy.Expressions
{
    public class Translator : ITranslator
    {
        private readonly Dictionary<string, string> _dictionary;

        public Translator(Dictionary<string, string> dictionary            )
        {
            _dictionary = dictionary;
        }

        public string Translate(string expression)
        {
            var translation = string.Empty;
            var expressionComponents = expression.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var word in expressionComponents)
            {
                translation += _dictionary[word] + " ";
            }
            return translation.Trim();
        }
    }
}